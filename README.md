# README #

This repository is to learn the features in Boost Graph library. 

### In the ToDo Lists ###

* Examples to learn the basic boost graph features
* Examples to learn the Boykov max-flow graph cut algorithm
* Use this to make an easy dynamic programming example
* Loop Belief propagation 
* A MCMC algorithm

### Contribution guidelines ###

* Tests with Google-test